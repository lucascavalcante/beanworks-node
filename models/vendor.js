'use strict';
module.exports = (sequelize, DataTypes) => {
  const Vendor = sequelize.define('Vendor', {
    ContactID: DataTypes.STRING,
    ContactStatus: DataTypes.STRING,
    Name: DataTypes.STRING,
    Address: DataTypes.STRING,
    Phone: DataTypes.STRING,
    UpdatedDateUTC: DataTypes.DATE,
    IsSupplier: DataTypes.BOOLEAN,
    IsCustomer: DataTypes.BOOLEAN,
    HasAttachments: DataTypes.BOOLEAN,
    HasValidationErrors: DataTypes.BOOLEAN
  }, {});
  Vendor.associate = function(models) {
    // associations can be defined here
  };
  return Vendor;
};