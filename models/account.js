'use strict';
module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define('Account', {
    AccountID: DataTypes.STRING,
    Code: DataTypes.STRING,
    Name: DataTypes.STRING,
    Status: DataTypes.STRING,
    Type: DataTypes.STRING,
    TaxType: DataTypes.STRING,
    Description: DataTypes.STRING,
    Class: DataTypes.STRING,
    SystemAccount: DataTypes.STRING,
    EnablePaymentsToAccount: DataTypes.BOOLEAN,
    ShowInExpenseClaims: DataTypes.BOOLEAN,
    BankAccountType: DataTypes.STRING,
    ReportingCode: DataTypes.STRING,
    ReportingCodeName: DataTypes.STRING,
    HasAttachments: DataTypes.BOOLEAN,
    UpdatedDateUTC: DataTypes.DATE
  }, {});
  Account.associate = function(models) {
    // associations can be defined here
  };
  return Account;
};