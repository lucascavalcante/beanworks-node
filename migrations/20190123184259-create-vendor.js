'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Vendors', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ContactID: {
        type: Sequelize.STRING
      },
      ContactStatus: {
        type: Sequelize.STRING
      },
      Name: {
        type: Sequelize.STRING
      },
      Address: {
        type: Sequelize.STRING
      },
      Phone: {
        type: Sequelize.STRING
      },
      UpdatedDateUTC: {
        type: Sequelize.DATE
      },
      IsSupplier: {
        type: Sequelize.BOOLEAN
      },
      IsCustomer: {
        type: Sequelize.BOOLEAN
      },
      HasAttachments: {
        type: Sequelize.BOOLEAN
      },
      HasValidationErrors: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Vendors');
  }
};