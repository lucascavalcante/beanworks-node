'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Accounts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      AccountID: {
        type: Sequelize.STRING
      },
      Code: {
        type: Sequelize.STRING
      },
      Name: {
        type: Sequelize.STRING
      },
      Status: {
        type: Sequelize.STRING
      },
      Type: {
        type: Sequelize.STRING
      },
      TaxType: {
        type: Sequelize.STRING
      },
      Description: {
        type: Sequelize.STRING
      },
      Class: {
        type: Sequelize.STRING
      },
      SystemAccount: {
        type: Sequelize.STRING
      },
      EnablePaymentsToAccount: {
        type: Sequelize.BOOLEAN
      },
      ShowInExpenseClaims: {
        type: Sequelize.BOOLEAN
      },
      BankAccountType: {
        type: Sequelize.STRING
      },
      ReportingCode: {
        type: Sequelize.STRING
      },
      ReportingCodeName: {
        type: Sequelize.STRING
      },
      HasAttachments: {
        type: Sequelize.BOOLEAN
      },
      UpdatedDateUTC: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Accounts');
  }
};