# Beanworks API

Clone the following repository
```
git clone https://lucascavalcante@bitbucket.org/lucascavalcante/beanworks-node.git
```

Access the folder
```
cd beanworks-node
```

Install dependencies
```
npm install
```

Run the migrations
```
node_modules/.bin/sequelize db:migrate
```

Run the server
```
npm start
```

Endpoints
```
GET /accounts

List all accounts from Xero
```

```
GET /vendors

List all vendors from Xero
```