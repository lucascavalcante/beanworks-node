const express = require('express');
const session = require('express-session');
const XeroClient = require('xero-node').AccountingAPIClient;
const config = require('./config/xero.json');

const PORT = 3000;
const HOST = '0.0.0.0';

const app = express();
const xero = new XeroClient(config);

app.use(session({
    secret: 'ThatIsAwesome',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}))

app.get('/', async (req, res) => {
    const oauth_verifier = req.query.oauth_verifier;
    const savedRequestToken = {
        oauth_token: req.session.oauthRequestToken.oauth_token,
        oauth_token_secret: req.session.oauthRequestToken.oauth_token_secret
    };
    const accessToken = await xero.oauth1Client.swapRequestTokenforAccessToken(savedRequestToken, oauth_verifier);
    req.session.accessToken = accessToken;
    if(req.session.returnTo)
        res.redirect(req.session.returnTo);
    res.send({ message: "Logged" });
});

app.get('/login', async (req, res) => {
    const requestToken = await xero.oauth1Client.getRequestToken();
    req.session.oauthRequestToken = requestToken;
    authUrl = xero.oauth1Client.buildAuthoriseUrl(requestToken);
    res.redirect(authUrl);
});

app.get('/accounts', async (req, res) => {
    if (req.session.accessToken) {
        const result = await xero.accounts.get();
        console.log(result);
        res.send({ accounts_number: result.Accounts.length });
    } else {
        authorizeRedirect(req, res, '/accounts');
    }
});

app.get('/vendors', async (req, res) => {
    if (req.session.accessToken) {
        const result = await xero.contacts.get();
        console.log(result);
        res.send({ contacts_number: result.Contacts.length });
    } else {
        authorizeRedirect(req, res, '/vendors');
    }
});

authorizeRedirect = async (req, res, returnTo) => {
    let requestToken = await xero.oauth1Client.getRequestToken();
    var authoriseUrl = xero.oauth1Client.buildAuthoriseUrl(requestToken);
    req.session.oauthRequestToken = requestToken;
    req.session.returnTo = returnTo;
    res.redirect(authoriseUrl);
}

app.listen(PORT, HOST);